# 0. Zen

import this

# 1. Типы не пишутся, но всёравно существуют!
a = True
b = 'some string'
c = 1 / 2

type(a)
type(b)
type(c)

b + c # Это не javascript. Типизация строгая, число само строкой не станет
str(b) + c # Вот так вот лучше

# 2. None!
smth = None
if smth is None:
    print('smth None')

if smth is not None:
    pass
else:
    pass

def f():
    pass

print(f())

type(None)

# 3. Область видимости переменых, global
x = 10

def f():
    print(x)

f()

x = 10
def f():
    print(x)
    x += 1  # error!

f()

# Сделано чтобы нельзя было случайно изменить что-то вне функции..
a = []
def f():
    a.append(123)
    print(a)

f()
# но это всёравно работает

x = 10

def f():
    global x # Теперь всё ок
    print(x)
    x += 1

f()

# 4. Лямбды
f = lambda a, b: a + b
f(10, 5)

list(map(lambda x: x**3, range(5)))

map(int, '123 33 334 545 4545 4545'.split())

def make_incrementor(n):
    return lambda x: x + n

inc10 = make_incrementor(10)

inc10(42)
inc10(32)

# НЕОБЯЗАТЕЛЬНО
# Различное кол-во аргументов
def cheeseshop(kind, *arguments, **keywords):
    print("-- Do you have any", kind, "?")
    print("-- I'm sorry, we're all out of", kind)
    for arg in arguments:
        print(arg)
    print("-" * 40)
    keys = sorted(keywords.keys())
    for kw in keys:
        print(kw, ":", keywords[kw])

# argument unpack
cheeseshop("Limburger", "It's very runny, sir.",
           "It's really very, VERY runny, sir.",
           shopkeeper="Michael Palin",
           client="John Cleese",
           sketch="Cheese Shop Sketch")



