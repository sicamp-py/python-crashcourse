# Functions

# C++
# int func(int arg1, float arg2=10.0) {
#    return 1;
# }

def func(arg1, arg2):
    return 1

def fib(n = 5):
    numbers = [0, 1]
    for i in range(n):
        numbers.append(numbers[-1] + numbers[-2])
    return numbers

f = fib

print(f)
print(f())
print(f(5))

def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise ValueError('invalid user response')
        print(reminder)

ask_ok() # Fail
ask_ok('Супермен это саломёт?')
ask_ok('Супермен это птиц?', reminder='Кнопка не та')


def f(a, L=[]):
    L.append(a)
    return L

print(f(1)) # [1]
print(f(2)) # ??
print(f(3)) # ???

# Better
def f(a, L=None):
    if L is None:
        L = []
    L.append(a)
    return L