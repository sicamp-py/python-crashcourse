# 2. Строки

# Строки могут быть как в одинарных кавычках, так и в двойных
"like this"
'and like that'

# Длинные строки
"""
Very long
text
on muliple lines
"""

a = 'bat'
b = 'man'

a + b # Можно складывать

word = 'Python'
word[0] # нумерация с нуля
word[3]

word[-1]
word[-2]

# Подстроки, слайсинг
word[1:4]
word[:2]
word[2:]
word[:2] + word[2:]

word[-2:] # ?

# bad index
word[-7] # exception
word[4:42] # ?
word[42:]  # ?

# Строки неизменяемые!
word[3] = 'J' # Exception!

# Длина строки
len(word)

# word. # нажмите tab
word.lower()
word.upper()
word.capitalize()
word.isnumeric()

test = 'Настя варит борщ'
test.startswith('Настя')
test.endswith('борщ')
# и ещё много всего

# Форматирование строк

print('{0} {1:.5f}'.format(word, 1 / 2))
# Подробнее смотрите примеры в документации https://docs.python.org/3.6/library/string.html#formatstrings

# А ещё разделение
'aba caba    daba mamba'.split()
# Разделение по символу
'aba\ncaba\ndaba'.split('\n')
#а ещё соединение
'aAa'.join(['b', 't', 'm', 'n'])
# а ещё можно убрать пробелы лишние с концов
'    dddd  xx dd     '.strip()