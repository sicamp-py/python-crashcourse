# Bad
f = open()
a, b = f.readline().strip().split()
lines = f.read().strip().split('\n')
f.close()

# Better
with open('output.txt', 'w') as f:
    f.write('some string')

print('test')

# Встроенная документация
help('test'.strip)

import math
help(math)

help('string')
