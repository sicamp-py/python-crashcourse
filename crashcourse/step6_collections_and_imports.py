# Collections

#lists
a = [1, 2, 3, 4, 5]
a.insert(2, 100)
a.extend([201, 10, 202, 203, 10])
a.count(10)
a.index(10)
a.remove(201)
del a[0]
a.reverse()
a.sort()
a.clear()

stack = []
stack.append(10)
stack.pop()

# Bad
squares = []
for x in range(10):
    squares.append(x**2)

# Better
squares = [x**2 for x in range(10)]

# А ещё можно так:
[(x, y) for x in [1,2,3]
       for y in [3,1,4]
        if x != y]

# Словари
birth_years = dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])

birth_years = {
    'Пушкин' : 1799,
    'Лермонтов' : 1814,
    'Маяковский' : 1893,
}

birth_years.keys()
birth_years.values()
birth_years['Я.Поэт'] = 2015
birth_years['Пушкин']
birth_years['кукушкин']
birth_years.get('кукушкин')
birth_years.get('кукушкин', 1999)
del birth_years['Я.Поэт']

for key, value in birth_years.items():
    print('{key} родился в {value}'.format(key=key, value=value))

# А ещё можно так:
t = {x: x ** 2 for x in (2, 4, 6)}

# Sets
a = [1, 2, 3, 1, 1, 10, 2, 3]
s = set(a)

a = set('abracadabra')
b = set('alacazam')
a - b
a | b
a & b
a ^ b

# Collections
from collections import Counter

c = Counter('aaabacabdadaadfdfdf')
c.most_common(2)

# Imports
import math
math.cos(math.pi)
from math import *
sin(pi)
