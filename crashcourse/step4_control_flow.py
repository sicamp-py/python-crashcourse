# Control flow

# Фиббоначи
a, b = 0, 1 # вот такая вот фишечка
while b < 10:      # двоеточие - вместо { в С++
    print(b)       # 4 пробела
    a, b = b, a+b

"""
Мой стих
    трудом
        громаду лет прорвёт
и явится
    весомо,
        грубо,
            зримо,
как в наши дни
    вошёл водопровод,
сработанный
    ещё рабами Рима.
"""
# Маяковский тоже любил отступы, будьте как Маяковский

# while - уже видели
# do {} while - НЕТ!

x = int(input('Число?'))
if x < 0:
    print('<0')
elif x == 0:
    print('just 0')
elif x == 1:
    print('another case')
else:
    print('больше')

# switch case - НЕТ

# for
words = ['cat', 'dog', 'window']
for word in words:
    print(word, len(word))

for word in words:
    #words.append(word.upper()) ## INFINITE LIST
    pass

for i in range(10):
    print(i**2)

list(range(1, 20))
list(range(20, 1, -1))

# BAD
for i in range(len(words)):
    print(i, words[i])

# Better
for i, word in enumerate(words):
    print(i, word)

# Поиск простых чисел
primes = [2]
for x in range(3, 10000):
    has_divider = False
    for y in primes:
        if x % y != 0:
            has_divider = True
            break
    if not has_divider:
        primes.append(x)

# Pass
while True:
    pass     # Ничего не делает =)
