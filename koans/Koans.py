import unittest

class Koans(unittest.TestCase):
    """В этом классе нужно заменить подчеркивания на какой-то код, чтобы тесты заработали.
    Запуск тестов: правой кнопкой по имени класса -> Run Unittests in 'Koans'"""

    def test_arithmetics(self):
        self.assertEqual(__, 2 + 2 * 2)
        self.assertEqual(__, 2 ** 3 ** 4)
        self.assertEqual(__, 5 / 2)
        self.assertEqual(__, 5 // 2)
        self.assertEqual(__, 3 * 7 % 5)

    def test_strings_basics(self):
        self.assertEqual(__, "abc" + "def")
        self.assertEqual(__, 'ab' + 'cd')
        self.assertEqual(__, 'ab' * 4)
        self.assertEqual(__, len('doesn\'t\twork'))
        self.assertEqual(__, len(r'doesn\'t\twork'))
        self.assertEqual(__, "Hi, {0}. My name is {1}.".format("Python", __))
        self.assertEqual(__, "Hello, Billy"[2:7])

    def test_lists_basics(self):
        self.assertListEqual(__, [1, 'ab'] + ['cd', [2, 3]])
        self.assertListEqual(__, [2, 1] * 3)
        self.assertEqual(__, len([2, 3, 'cd', 2.3, [3, 4]]))

        list0 = [2, 3, 4]
        list0.__()
        self.assertListEqual([2, 3], list0)

        list1 = [2, 3, 4]
        list1.append(__)
        self.assertEqual(14, sum(list1))
        list1[2] = __
        self.assertEqual(0, min(list1))

        list2 = __
        self.assertEqual(300, len(list2))

        list3 = [5, 4, 3, 2, 1]
        self.assertListEqual([3, 2], list3[__:__])
        self.assertListEqual([3, 4, 5], list3[__::__])

        list4 = [[16, 23, 42], [4, 8, 15]]
        self.assertEqual(__, list4[1][2])

        list5 = [1, 2, 3, 4, 5]
        del(list5[__])
        self.assertListEqual([1, 2, 4, 5], list5)

        list6 = [2, 1, 3, 1, 3, 1, 5]
        list6.remove(__)
        self.assertListEqual([2, 1, 1, 3, 1, 5], list6)

        list7 = [2, 3, 1, 5, 4]
        sorted(list7)
        self.assertListEqual(__, list7)
        self.assertListEqual(__, sorted(list7))
        list7.sort()
        self.assertListEqual(__, list7)

        s = 0
        for i, v in enumerate([4, 5, 6]):
            s += i * v
        self.assertEqual(__, s)


    def test_tuples(self):
        tuple = (3, 4, 5)
        a, b, c = tuple
        self.assertEqual(_, a)
        self.assertEqual(_, b)
        self.assertEqual(_, c)
        self.assertEqual(_, tuple[2])
        self.assertEqual(_, tuple[1])
        self.assertEqual(_, tuple[0])
        self.assertEqual(_, len(tuple))
        # tuple[0] = 1   #incorrect, tuples are immutable

    def is_leap_year(self, year):
        __

    def test_if_basics(self):
        self.assertTrue(self.is_leap_year(2008))
        self.assertFalse(self.is_leap_year(2007))
        self.assertFalse(self.is_leap_year(2006))
        self.assertFalse(self.is_leap_year(2010))
        self.assertFalse(self.is_leap_year(2100))
        self.assertTrue(self.is_leap_year(2000))
        self.assertTrue(self.is_leap_year(2400))

    def test_ranges(self):
        self.assertListEqual(__, list(range(3)))
        self.assertListEqual(__, list(range(3, 6)))
        self.assertListEqual(__, list(range(6, 3, -1)))

    def is_prime(self, number):
        __

    def test_for(self):
        self.assertFalse(self.is_prime(1))
        self.assertTrue(self.is_prime(2))
        self.assertTrue(self.is_prime(3))
        self.assertFalse(self.is_prime(4))
        self.assertTrue(self.is_prime(5))
        self.assertFalse(self.is_prime(9))
        self.assertTrue(self.is_prime(11))
        self.assertTrue(self.is_prime(13))
        self.assertFalse(self.is_prime(1001))
        self.assertTrue(self.is_prime(10**9 + 9))
        self.assertTrue(self.is_prime(10**9 + 7))
        self.assertFalse(self.is_prime(10**9 + 5))

        s = 0
        for x in [1, 2, 3]:
            _ += _
        self.assertEqual(6, s)

        for x in (4, 5, 6):
            _ += _
        self.assertEqual(21, s)

    #___реализуйте функцию gcd (алгоритм Евклида нахождения НОД) вместо этого комментария___#

    def test_gcd(self):
        self.assertEqual(1, self.gcd(3, 5))
        self.assertEqual(6, self.gcd(24, 18))
        self.assertEqual(1, self.gcd(1, 0))
        self.assertEqual(33, self.gcd(66, 99))
        self.assertEqual(1, self.gcd(317811,514229))
        self.assertEqual(92, self.gcd(92, 92))

    def test_set(self):
        set1 = set()
        self.assertEqual(__, len(set1))
        set1.add(__)
        self.assertTrue(5 in set1)
        set1.add(__)
        self.assertEqual(2, len(set1))
        set1.remove(__)
        self.assertFalse(5 in set1)

        set2 = {2, 3, 4}
        set3 = {3, 4, 5}
        self.assertSetEqual(__, set2.intersection(set3))
        self.assertSetEqual(__, set2.union(set3))
        self.assertSetEqual(__, set2.difference(set3))
        self.assertSetEqual(__, set3.difference(set2))
        self.assertSetEqual(__, set2.symmetric_difference(set3))
        self.assertSetEqual(__, set3.symmetric_difference(set2))

        lst = list()
        for x in set('abracadabra'):
            lst.append(x)
        self.assertCountEqual([__], lst)
        lst.clear()
        for x in sorted(set('abracadabra')):
            lst.append(x)
        self.assertListEqual(__, lst)

    def test_dict(self):
        dict1 = {'apples': 5, 'oranges': 6}
        self.assertEqual(__, dict1['apples'])
        self.assertFalse(__ in dict1)
        dict1['bananas'] = 12
        s = 0
        for k, v in dict1.items():
            s += len(k) * v
        self.assertEqual(__, s)